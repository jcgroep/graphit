<?php namespace Jcgroep\GraphIt\Graphs;

use Jcgroep\GraphIt\Models\FluentObject;
use Jcgroep\GraphIt\Models\JsonBuilder;

class BaseGraph extends FluentObject
{
    use JsonBuilder;

    protected $id = 'chartDiv';

    public function getId()
    {
        return $this->id;
    }


    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

}