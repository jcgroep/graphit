<?php namespace Jcgroep\GraphIt\Graphs;


class HundredPercentStackedGraph extends SerialGraph
{



    protected function getValueAxes()
    {
        return $this->arrayToJsonArray([
            $this->arrayToJsonObject([
                'stackType' => '100%',
                'axisAlpha' => 0,
                'gridAlpha' => 0,
                'labelsEnabled' => true,
                'position' => 'left'
            ])
        ]);
    }

    protected function getGraphs()
    {
        $valueBlocks = $this->getColumns();
        array_shift($valueBlocks); //Remove the first item which is the title of this graph
        $rows = [];
        sort($valueBlocks);
        foreach ($valueBlocks as $value) {
            $rows[] = $this->arrayToJsonObject([
                'balloonText' => '[[title]]<br><span style=\'font-size:14px;\'><b>[[value]]</b> ([[percents]]%)</span>',
                'fillAlphas' => '0.9',
                'lineAlpha' => '0.5',
                'labelText' => '[[percents]]%',
                'type' => 'column',
                'title' => $value,
                'valueField' => $value,
                'hidden' => in_array($value, $this->hiddenColumns)
            ]);
        }
        return $this->arrayToJsonArray($rows);
    }
}