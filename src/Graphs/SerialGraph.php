<?php namespace Jcgroep\GraphIt\Graphs;

use Jcgroep\GraphIt\Tables\Table;

abstract class SerialGraph extends BaseGraph
{
    /**
     * @var $data Table
     */
    protected $data;
    protected $category;
    /**
     * @var $columns array
     */
    protected $columns;

    protected $height = '500px';
    protected $bottomAxisTitle = '';
    protected $valueTitle = '';
    protected $drawLegend = true;
    protected $hiddenColumns;
    protected $colors = ['#91C400', '#3598dc', 'purple', 'red', 'orange', 'yellow'];


    public function withColors(array $colors)
    {
        $this->colors = $colors;
        return $this;
    }

    public function withoutLegend()
    {
        $this->drawLegend = false;
        return $this;
    }

    public function hideGraphs($hiddenColumns)
   {
       $this->hiddenColumns = $hiddenColumns;
       return $this;
   }

    public function withValueTitle($title)
    {
        $this->valueTitle = $title;
        return $this;
    }

    public function withBottomAxisTitle($title)
    {
        $this->bottomAxisTitle = $title;
        return $this;
    }

    public function withHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    public function onlyColumns(array $array)
    {
        $this->columns = $array;
        return $this;
    }

    public function withData(Table $data)
    {
        $this->data = $data;
        return $this;
    }

    public function getColumns()
    {
        if(isset($this->columns)){
            return [$this->category => $this->category] + $this->columns;
        }
        return $this->data->collapse()->keys()->toArray();
    }

    public function withCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    protected function getLegend()
    {
        return $this->arrayToJsonObject([
            'enabled' => $this->drawLegend,
            'autoMargins' => false,
            'borderAlpha' => 0.2,
            'equalWidths' => false,
            'horizontalGap' => 10,
            'markerSize' => 10,
            'useGraphSettings' => true,
            'valueAlign' => 'left',
            'valueWidth' => 0,
            'marginTop' => 20,
            'marginBottom' => 20,
        ]);
    }

    protected function getValueAxes()
    {
        return $this->arrayToJsonArray([
            $this->arrayToJsonObject([
                'labelsEnabled' => true,
                'position' => 'left',
                'title' => $this->valueTitle
            ])
        ]);
    }

    protected function getCategoryAxes()
    {
        return $this->arrayToJsonObject([
            'gridPosition' => 'start',
            'axisAlpha' => 0,
            'gridAlpha' => 0,
            'title' => $this->bottomAxisTitle
        ]);
    }

    protected function dataToString()
    {
        $rows = [];
        foreach ($this->data->toArray() as $location) {
            $rows[] = $this->arrayToJsonObject(array_intersect_key($location, array_flip($this->getColumns())));
        }
        return $this->arrayToJsonArray($rows);
    }

    protected function getGraphs()
    {
        $valueBlocks = $this->getColumns();
        array_shift($valueBlocks); //Remove the first item which is the title of this graph
        $rows = [];
        foreach ($valueBlocks as $value) {
            $rows[] = $this->arrayToJsonObject([
                'fillAlphas' => '0.9',
                'lineAlpha' => '0.5',
                'type' => 'column',
                'title' => $value,
                'valueField' => $value
            ]);
        }
        return $this->arrayToJsonArray($rows);
    }

    public function getGraphJavascript()
    {
        $javascript = 'var '. $this->id . ' = AmCharts.makeChart("' . $this->id . '", ' . $this->arrayToJsonObject([
            'type' => 'serial',
            'theme' => 'light',
            'categoryField' => $this->category,
            'legend' => $this->getLegend(),
            'dataProvider' => $this->dataToString(),
            'marginLeft' => 0,
            'autoMarginOffset' => 0,
            'valueAxes' => $this->getValueAxes(),
            'categoryAxis' => $this->getCategoryAxes(),
//            'colors' => $this->arrayToJsonArray($this->colors),
            'graphs' => $this->getGraphs()
        ]) . ');

            $(document).on("shown", ".modal", function(){
                ' . $this->id . '.invalidateSize();
            });';
        $javascript .= $this->fireEvents();

        return $javascript;
    }

    public function getGraphDiv()
    {
        return '<div id="'. $this->id . '" class="col-md-12" style="height: ' . $this->height . '; padding: 0;"></div>';
    }

    private function fireEvents()
    {
        if($this->drawLegend == false){
            return '';
        }
        return "
            var getHiddenColumns = function(chart){
                var hidden = [];
                $.each(chart.graphs, function(index){
                    if(chart.graphs[index].hidden){
                        hidden.push(chart.graphs[index].title);
                    }
                });
                return hidden;
            }

        setTimeout(function(){
            $this->id.legend.addListener('hideItem', function (event) {
                var changeEvent = jQuery.Event( 'change' );
                changeEvent.hiddenGraphs = getHiddenColumns(event.chart);
                $($this->id).trigger(changeEvent);
            });
            $this->id.legend.addListener('showItem', function (event) {
                var changeEvent = jQuery.Event( 'change' );
                changeEvent.hiddenGraphs = getHiddenColumns(event.chart);
                $($this->id).trigger(changeEvent);
            });
        }, 1000);";
    }
}