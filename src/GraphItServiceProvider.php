<?php

namespace Jcgroep\GraphIt;

use Illuminate\Support\ServiceProvider;

class GraphItServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'GraphIt');
        $this->loadTranslationsFrom(__DIR__ . '/lang', 'GraphIt');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {

    }
}