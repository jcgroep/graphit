<?php namespace Jcgroep\GraphIt\Converters;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Lang;

class WeekConverter extends BaseConverter
{

    protected $data;
    protected $dateColumn = 'date';
    protected $valueColumn = 'value';
    protected $weeks;

    public function withData(Collection $data)
    {
        $this->data = $data;
        return $this;
    }

    public function withDateColumn($dateColumn)
    {
        $this->dateColumn = $dateColumn;
        return $this;
    }

    public function withValueColumn($valueColumn)
    {
        $this->valueColumn = $valueColumn;
        return $this;
    }

    public function forNumberOfWeeks($weeks)
    {
        $this->weeks = $weeks;
        return $this;
    }

    public function toJson()
    {
        $data = [];
        foreach(range(0, $this->weeks-1) as $week){
            $data[Carbon::now()->subWeeks($this->weeks - $week)->weekOfYear] = [];
        }
        foreach ($this->data as $day) {
            $date = Carbon::parse($day[$this->dateColumn]);
            $data[$date->weekOfYear][trans('dates.abbreviations.' . $date->format('w'))] = $day[$this->valueColumn];
        }
        $json = new Collection();
        foreach ($data as $week => $weekData) {
            $week = [trans('dates.Week') => $week];
            foreach(trans('dates.abbreviations') as $day){
                $week[$day] = (array_key_exists($day, $weekData) ? $weekData[$day] : 0);
            }
            $week[ucfirst(trans('global.total'))] = array_sum($weekData);
            $json->push($week);
        }
        return $json;
    }
}