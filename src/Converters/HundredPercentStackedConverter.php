<?php namespace Jcgroep\GraphIt\Converters;


use Jcgroep\GraphIt\SearchServices\HundredPercentStackedSearchService;
use Illuminate\Support\Collection;
use Jcgroep\GraphIt\Tables\Table;

class HundredPercentStackedConverter extends BaseConverter
{
    /**
     * @var $searchService HundredPercentStackedSearchService
     */
    protected $searchService;

    public function withSearchService(HundredPercentStackedSearchService $searchService)
    {
        $this->searchService = $searchService;
        return $this;
    }

    /*
     * @return Table
     */
    public function toJson()
    {
        $json = [];
        $data = $this->searchService
            ->query()
            ->get();
        $row = $this->searchService->getChartColumn();

        foreach($data as $location) {
            $location = (array)$location;
            if(!array_key_exists($location[$row], $json)){
                $json[$location[$row]] = [$row => $location[$row]];
            }
            $json[$location[$row]][$location[$this->searchService->getColumn()]] = $location['count'];
        }
        $collection = new Table($json);
        return $collection;
    }
}