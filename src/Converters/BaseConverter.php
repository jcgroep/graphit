<?php namespace Jcgroep\GraphIt\Converters;

use Jcgroep\GraphIt\Models\FluentObject;
use Jcgroep\GraphIt\Models\GraphData;
use Jcgroep\GraphIt\Models\TableData;

abstract class BaseConverter extends FluentObject
{
    public abstract function toJson();
}