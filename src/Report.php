<?php namespace Jcgroep\GraphIt;

use Jcgroep\GraphIt\Models\FluentObject;

abstract class Report extends FluentObject
{
    protected $params;

    public abstract function getReportingObjects();

    /**
     * @param array $params
     * @return $this static
     */
    public function withParams(array $params)
    {
        $this->params = $params;
        return $this;
    }
}