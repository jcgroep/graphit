<?php namespace Jcgroep\GraphIt\SearchServices;

use Illuminate\Support\Facades\DB;
use Jcgroep\GraphIt\Models\FluentObject;
use Jcgroep\GraphIt\Models\TableData;

class GeneralSearchService extends FluentObject
{
    protected $table;

    /**
     * @param mixed $table
     * @return static
     */
    public function forTable($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return DB::table($this->table);
    }
}