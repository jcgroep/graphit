<?php namespace Jcgroep\GraphIt\SearchServices;


use DB;

class HundredPercentStackedSearchService extends GeneralSearchService
{
    protected $column;
    protected $chartColumn;
    protected $sums = [];

    /**
     * @return mixed
     */
    public function getColumn()
    {
        return $this->column;
    }

    protected $name;

    /**
     * @return mixed
     */
    public function getChartColumn()
    {
        return $this->chartColumn;
    }

    /**
     * @param string $column
     * @return self
     */
    public function stacksByColumn($column)
    {
        $this->column = $column;
        return $this;
    }

    public function withChartColumn($column)
    {
        $this->chartColumn = $column;
        return $this;
    }

    public function withSum($column)
    {
        $this->sums[] = $column;
        return $this;
    }

    public function query()
    {
        $query = parent::query();

        if(isset($this->column)){
            $query->groupBy($this->column);
        }

        if (isset($this->chartColumn)) {
            $query->groupBy($this->chartColumn);
        }

        if (!empty($this->sums)) {
            $selectStatement = $this->chartColumn . ', ' . $this->column;
            foreach ($this->sums as $sum) {
                $selectStatement .= ', ' . 'SUM(' . $sum . ') as ' . $sum;
            }
            $query->select($selectStatement);
        } else {
            $query->select(DB::raw($this->chartColumn . ', ' . $this->column . ', ' . 'count(*) as count'));
        }

        return $query;
    }
}