<?php namespace Jcgroep\GraphIt\Models;

class FluentObject
{
    /**
     * @return static static
     */
    public static function make()
    {
        return new static;
    }
}