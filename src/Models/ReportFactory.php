<?php namespace Jcgroep\GraphIt\Models;

use Jcgroep\GraphIt\Report;
use ReflectionClass;

abstract class ReportFactory
{
    /**
     * @param $report
     * @return Report
     */
    public static function get($report)
    {
        if(in_array($report, self::all())){
            return $report::make();
        }
        throw new \InvalidArgumentException('Report ' . $report . ' not found');
    }

    public static function all(){
        $oClass = new ReflectionClass(get_called_class());
        return $oClass->getConstants();
    }
}