<?php namespace Jcgroep\GraphIt\Models;

use Illuminate\Support\Str;

trait JsonBuilder
{
    /**
     * @param $array String|array The array to convert to json or a to json converted string
     * @return string
     */
    public function arrayToJsonObject($array)
    {
        $string = '{';
        foreach ($array as $key => $value) {
            if(is_string($value) && !$this->valueIsJson($value) && !$this->valueIsJavascript($value) && $value != 'undefined') {
                $string .= '"' . $key . '": "' . $value . '", ';
            }elseif(is_bool($value)) {
                $string .= '"' . $key . '": ' . ($value ? 'true': 'false') . ', ';
            }else{
                $string .= '"' . $key . '": ' . $value . ', ';
            }
        }
        $string = rtrim($string, ', ');

        return $string . '}';
    }

    /**
     * @param $array String|array The array to convert to json or a to json converted string
     * @return string
     */
    public function arrayToJsonArray($array)
    {
        $string = '[';
        foreach ($array as $value) {
            if (!is_string($value) || $this->valueIsJson($value)) {
                $string .= $value . ', ';
            } else {
                $string .= '"' . $value . '", ';
            }
        }
        $string = rtrim($string, ', ');

        return $string . ']';
    }

    /**
     * Check whether a given value is a json array or object
     * @param $value
     * @return bool
     */
    public function valueIsJson($value)
    {
        return in_array(substr($value,0,1), ['[', '{']) && in_array(substr($value,-1,1), [']', '}']);
    }

    public function valueIsJavascript($value)
    {
        return Str::startsWith($value, 'function');
    }
}