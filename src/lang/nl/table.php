<?php
return [
    'less' => 'minder',
    'more' => 'meer',
    'noDataAvailable' => 'Geen data beschikbaar',
];