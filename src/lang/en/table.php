<?php
return [
    'less' => 'less',
    'more' => 'more',
    'noDataAvailable' => 'No data available',
];