<?php namespace Jcgroep\GraphIt\Tables;

use Illuminate\Support\Collection;

class HtmlRow
{
    /**
     * @var mixed
     */
    private $rowData;
    private $keys;

    /**
     * Row constructor.
     * @param mixed $rowData
     */
    public function __construct($rowData, $keys)
    {
        if($rowData instanceof Collection){
            $rowData = $rowData->toArray();
        }
        $this->rowData = $rowData;
        $this->keys = $keys;
    }

    public function draw()
    {
        $html = '<tr>';
        foreach($this->keys as $key => $options){
            $html .= '<td class="' . (array_key_exists('columnClass', $options) ? $options['columnClass'] : '')  . '">';
            if(array_key_exists($key, $this->rowData)){
                $value = $this->rowData[$key];
            }else{
                $value = '0';
            }
            if(array_key_exists('valueType', $options)){
                switch ($options['valueType']){
                    case 'money':
                        $html .= '&#8364; ' . number_format(round($value, 0), 0, ',', '.');
                        break;
                }
            }else{
                $html .= $value;
            }
            $html .= '</td>';
        }
        return $html . '</tr>';
    }
}