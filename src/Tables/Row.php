<?php namespace Jcgroep\GraphIt\Tables;

use Illuminate\Support\Collection;

class Row
{
    /**
     * @var mixed
     */
    private $rowData;
    private $keys;

    /**
     * Row constructor.
     * @param mixed $rowData
     */
    public function __construct($rowData, $keys)
    {
        if($rowData instanceof Collection){
            $rowData = $rowData->toArray();
        }
        $this->rowData = $rowData;
        $this->keys = $keys;
    }

    public function draw()
    {
        $html = '<tr>';
        foreach($this->keys as $key => $options){
            $html .= '<td class="' . (array_key_exists('columnClass', $options) ? $options['columnClass'] : '')  . '">';
            if(array_key_exists($key, $this->rowData)){
                $html .= $this->rowData[$key];
            }else{
                $html .= '0';
            }
            $html .= '</td>';
        }
        return $html . '</tr>';
    }
}