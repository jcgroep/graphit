<?php namespace Jcgroep\GraphIt\Tables;

use Jcgroep\GraphIt\Models\JsonBuilder;
use Lang;

class TableTranslation
{
    use JsonBuilder;

    public function __toString()
    {
        return $this->arrayToJsonObject([
            'sLengthMenu' => '',
            'sZeroRecords' => trans('tables.NoResultsFound'),
            'sInfo' => trans('tables.sInfo'),
            'sInfoEmpty' => trans('tables.NoResultsFound'),
            'sInfoFiltered' => trans('tables.sInfoFiltered'),
            'sInfoPostFix' => '',
            'sSearch' => trans('tables.sSearch'),
            'sEmptyTable' => trans('tables.NoResultsFound'),
            'sInfoThousands' => '.',
            'processing' => trans('tables.Processing'),
            'oPaginate' => $this->getPaginationTranslation(),
        ]);
    }

    private function getPaginationTranslation()
    {
        return $this->arrayToJsonObject([
            'sFirst' => trans('tables.sFirst'),
            'sLast' => trans('tables.sLast'),
            'sNext' => trans('tables.sNext'),
            'sPrevious' => trans('tables.sPrevious')
        ]);
    }
}