<?php namespace Jcgroep\GraphIt\Tables;

use Jcgroep\GraphIt\Graphs\BaseGraph;

class HtmlTable extends Table
{
    protected $rows = [];
    /**
     * @var $header array
     */
    protected $header;
    protected $id = 'table';
    protected $class = '';
    protected $columns;
    protected $dependsOnGraph;

    /**
     * @param string $class
     * @return $this
     */
    public function withClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @param array $columns an array with the column name as key and an array of column options as value
     * Possible column options:
     *  - columnClass
     *
     * @return $this
     */
    public function withColumns(array $columns)
    {
        $this->columns = $columns;

        return $this;
    }

    public function dependsOn(BaseGraph $graph)
    {
        $this->dependsOnGraph = $graph;
        return $this;
    }

    public function withId($id)
    {
        $this->id = $id;

        return $this;
    }

    protected function getRowsHtml()
    {
        $html = '';
        foreach ($this->items as $rowData) {
            $html .= (new HtmlRow($rowData, $this->columns))->draw();
        }
        return $html;
    }

    public function draw()
    {
        $html = '<table class="table ' . $this->class . '" id=' . $this->id . '>';
        $html .= '<thead><tr>';
        foreach ($this->columns as $columnName => $options) {
            $html .= '<th class="' . (array_key_exists('columnClass', $options) ? $options['columnClass'] : '') . '">';
            $html .= $columnName;
            $html .= '</th>';
        }
        $html .= '</tr></thead><tbody>';
        $html .= $this->getRowsHtml();
        return $html . '</tbody></table>';
    }

    public function getJavascript()
    {
        if($this->dependsOnGraph){
            $graphId = $this->dependsOnGraph->getId();
            return "
                 $($graphId).on('change', function (event) {
                    $.ajax({
                        url: '" . route('report.update', [\ReportFactory::PROCES_INFORMATIE]) . "',
                        method: 'POST',
                        data: {
                            _token: $('meta[name=\"csrf-token\"]').attr('content'),
                            table: '$this->id',
                            hidden: event.hiddenGraphs
                        },
                        success: function(updatedTable){
                            $($this->id).html(updatedTable);
                        }
                    });
                });
            ";
        }
        return '';
    }
}