<?php namespace Jcgroep\GraphIt\Tables;

use Illuminate\Support\Collection;
use Lang;

class Table extends Collection
{

    protected $rotated = false;
    protected $rowSum = false;
    protected $colSum = false;
    protected $data;
    protected $withoutColumns = [];

    public function removeRows($columns)
    {
        $this->withoutColumns = $columns;
        return $this;
    }

    public function rotate()
    {
        $this->rotated = true;
        return $this;
    }

    public function withRowSum()
    {
        $this->rowSum = true;
        return $this;
    }

    public function withColSum()
    {
        $this->colSum = true;
        return $this;
    }

    protected function rotateData($data)
    {
        $rotatedData = [];
        foreach ($data as $locatie => $statusData) {
            array_shift($statusData);
            foreach ($statusData as $status => $value) {
                $rotatedData[$status][\trans('Status')] = $status;
                $rotatedData[$status][$locatie] = $value;
            }
        }
        return new Collection($rotatedData);
    }

    public function processData()
    {
        if ($this->rotated) {
            $this->items = $this->rotateData($this->items);
        }
        $this->items = $this->removeUnwantedColumns();

        if ($this->rowSum) {
            $this->items = $this->addRowSum($this->items);
        }
        if ($this->colSum) {
            $this->items = $this->addColSum($this->items);
        }
        ksort($this->items);
        return new Table($this->items);
    }

    protected function addRowSum($data)
    {
        if (!is_array($this->items)) {
            $this->items = $this->items->toArray();
        }
        $withRowSums = [];
        foreach ($data as $key => $value) {
            $value = (array) $value;
            $summables = array_filter($value, function ($var) {
                return is_string($var) == false;
            });
            $value[trans('procesInformatie.Total')] = array_sum($summables);
            $withRowSums[$key] = $value;
        }
        return $withRowSums;
    }

    protected function addColSum($data)
    {
        $data = Collection::make($data);
        $data = $data->map(function($value){return (array) $value;});
        $header = $data->collapse()->keys();
        $totals = [];
        foreach ($header as $columnName) {
            if (is_numeric($data->collapse()[$columnName])) {
                $totals[$columnName] = $data->sum($columnName);
            } else {
                $totals[$columnName] = trans('procesInformatie.Total');
            }
        }
        $data['6. ' . trans('procesInformatie.Total')] = $totals;
        return $data->toArray();
    }

    public function copy()
    {
        return clone $this;
    }

    protected function removeUnwantedColumns()
    {
        if (!is_array($this->items)) {
            $this->items = $this->items->toArray();
        }
        return array_diff_key($this->items, array_flip($this->withoutColumns));
    }

}