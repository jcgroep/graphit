<?php namespace Jcgroep\GraphIt\Tables;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Lang;

class DensityTable extends BaseTable
{
    protected $max = 0;
    protected $colors = ['#EEEEEE', '#DBE088', '#9BBF66', '#629B40', '#629B40'];

    public function withData(Collection $data)
    {
        $data = $this->onlyDays($data->reverse());
        foreach ($data as $item) {
            $max = max($item);
            if ($max > $this->max) {
                $this->max = $max;
            }
        }
        return parent::withData($data);
    }

    public function withColors(array $colors)
    {
        $this->colors = $colors;
        return $this;
    }

    protected function getRowsHtml()
    {
        $html = '';
        $first = true;
        $future = false;
        foreach ($this->data as $week => $rowData) {
            $html .= '<tr>';
            foreach ($rowData as $key => $cellData) {
                if (trans('dates.abbreviations')[\Carbon::now()->dayOfWeek] == $key && $first == true) {
                    $html .= '<td style="background-color: ' . $this->getColor($cellData) . '" data-date="' . $this->getDate($week, $key) . '"></td>';
//                    $html .= '<td style="border: 1px solid #494949; background-color: ' . $this->getColor($cellData) . '"></td>';
                    $future = true;
                } elseif($first && $future){
                    $html .= '<td style="background-color: transparent"></td>';
                }
                else {
                    $html .= '<td style="background-color: ' . $this->getColor($cellData) . '" data-date="' . $this->getDate($week, $key) . '"></td>';
                }
            }
            $html .= '</tr>';
            $first = false;
            $future = false;
        }
        return $html;
    }

    private function getColor($value)
    {
        return $this->colors[intval(ceil($value / ($this->max) * (count($this->colors) - 1)))];
    }

    private function onlyDays(Collection $data)
    {
        $subset = new Collection();
        foreach ($data as $row) {
            $subset->put($row['Week'], array_intersect_key($row, array_flip(trans('dates.abbreviations'))));
        }
        return $subset;
    }

    public function getLegend()
    {
        $html = '<p><span style="margin-right: 5px;">' . ucfirst(trans('GraphIt::table.less')) . '</span>';
        foreach($this->colors as $color){
            $html .= '<span style="margin-right: 5px; width: 14px; height: 14px; display:inline-block; background-color: ' . $color . '"></span>';
        }
        return $html . '<span">' . ucfirst(trans('GraphIt::table.more')) . '</span></p>';
    }

    /**
     * Get the date from a week and a day, doesn't work for future dates.
     * @param $week
     * @param $day
     * @return string
     */
    private function getDate($week, $day)
    {
        $daysToNumber = array_flip(trans('dates.abbreviations'));
        $daysToNumber[trans('dates.abbreviations.0')] = 7;
        $date = Carbon::now()->startOfYear()->addWeeks($week)->startOfWeek()->addDays($daysToNumber[$day] -1);
        if(($date->month < 3 && $date->weekOfYear > 40) ||  Carbon::now()->subWeeks($week)->endOfWeek()->year != Carbon::now()->year){
            $date->subYear();
        }
        return $date->format('d-m-Y');
    }

}