<?php namespace Jcgroep\GraphIt\Tables;

use ErrorException;
use Illuminate\Support\Collection;
use Jcgroep\GraphIt\Models\FluentObject;
use Lang;

class BaseTable extends FluentObject
{
    protected $rows = [];
    /**
     * @var $header array
     */
    protected $header;
    protected $id = 'table';
    protected $class = 'table';
    protected $columns;
    protected $data;

    /**
     * @param string $class
     * @return $this
     */
    public function withClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @param array $columns an array with the column name as key and an array of column options as value
     * Possible column options:
     *  - columnClass
     *
     * @return $this
     */
    public function withColumns(array $columns)
    {
        $this->columns = $columns;

        return $this;
    }

    public function withData(Collection $data)
    {
        if (!isset($this->columns)) {
            $columnNames = $data->collapse()->keys()->toArray();
            $this->columns = array_combine($columnNames, array_fill(0, count($columnNames), []));
        }
        $this->data = $data;

        return $this;
    }

    public function withId($id)
    {
        $this->id = $id;

        return $this;
    }

    protected function getRowsHtml()
    {
        $html = '';
        foreach ($this->data as $rowData) {
            $html .= (new Row($rowData, $this->columns))->draw();
        }
        return $html;
    }

    public function draw()
    {
        try{
            $html = '<table class="' . $this->class . '" id=' . $this->id . '>';
                    $html .= '<thead><tr>';
                    foreach ($this->columns as $columnName => $options) {
                        $html .= '<th class="' . (array_key_exists('columnClass', $options) ? $options['columnClass'] : '') . '">';
                        $html .= $columnName;
                        $html .= '</th>';
                    }
                    $html .= '</tr></thead><tbody>';
                    $html .= $this->getRowsHtml();
                    return $html . '</tbody></table>';
        }catch (ErrorException $e){
            return trans('GraphIt::table.noDataAvailable');
        }

    }
}