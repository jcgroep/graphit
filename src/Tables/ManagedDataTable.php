<?php namespace Jcgroep\GraphIt\Tables;

use Jcgroep\GraphIt\Models\JsonBuilder;

class ManagedDataTable extends BaseTable
{
    use JsonBuilder;

    protected $ajaxUrl;
    protected $pageLength = 10;
    protected $start = 0;

    public function getJavascript()
    {
        return '$("#' . $this->id . '").dataTable(' . $this->arrayToJsonObject([
            'pageLength' => $this->pageLength,
            'displayStart' => $this->start,
            'ordering' => false,
            'processing' => true,
            'serverSide' => true,
            'ajax' => $this->ajaxUrl,
            'language' => new TableTranslation(),
            'columnDefs' => $this->getColumnDefinitions(),
            'order' => $this->getInitialOrdering(),
            'oSearch' => $this->getInitialSearch(),
            'aoSearchCols' => $this->getInitialSearchColumns(),
        ]) . ')';
    }

    public function getColumnDefinitions()
    {
        return $this->arrayToJsonArray([
            $this->arrayToJsonObject([
                'orderable' => false,
                'targets' => $this->getOrderableColumns()
            ]),
            $this->arrayToJsonObject([
                'searchable' => false,
                'targets' => $this->getSearchableColumns()
            ])
        ]);
    }

    public function withAjaxUrl( $url )
    {
        $this->ajaxUrl = $url;
        return $this;
    }

    protected function getRowsHtml()
    {
        return '';
    }

    private function getOrderableColumns()
    {
        return $this->arrayToJsonArray([]);
    }

    private function getSearchableColumns()
    {
        return $this->arrayToJsonArray([]);
    }

    private function getInitialOrdering()
    {
        return $this->arrayToJsonArray([
            $this->arrayToJsonArray([
                1,
                'asc'
            ])
        ]);
    }

    protected function getInitialSearch()
    {
        return '{"sSearch": ""}';
    }

    protected function getInitialSearchColumns()
    {
        return '[' . join(',', array_fill(0,count($this->columns), 'null')) . ']';
    }
}