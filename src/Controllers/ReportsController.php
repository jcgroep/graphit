<?php namespace Jcgroep\GraphIt\Controllers;

use Illuminate\Routing\Controller;
use Input;
use Response;

class ReportsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function show($report)
	{
		return view('GraphIt::report', \ReportFactory::get($report)->withParams(Input::all())->getReportingObjects());
	}

    public function update($report)
    {
        $tableSelect = Input::get('table');
        $objects = \ReportFactory::get($report)
            ->withParams(Input::all())
            ->getReportingObjects();
        if(!array_key_exists($tableSelect, $objects)){
            throw new \InvalidArgumentException('Can not find the object: "' . $tableSelect . '", provide it with the input "table"');
        }
        $table = $objects[$tableSelect];
        return $table->draw();
    }

}
